async function fetchData() {
	let getData = await fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'GET'
	});
	let jsonData = await getData.json();
	let mapData = jsonData.map((object) => {
		return object.title
	})
	console.log(mapData);
}
fetchData(0);

async function fetchData1() {
	let getData1 = await fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'GET'
	})

	let jsonData1 = await getData1.json();
	console.log(jsonData1[0]);
	console.log(`The item "${jsonData1[0].title}" on the list has a status of ${jsonData1[0].completed}`);
}
fetchData1(0);

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"completed": false,
		"title": "Created to Do List Item",
		"userId": 1
	})
})
.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"dateCompleted": "Pending",
		"description": "To update the my to do list with a different data structure",
		"status": "Pending",
		"title": "Updated To Do List Item",
		"userId": 1
	})
})

.then(response => response.json())
.then(json => console.log(json));

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		"dateCompleted": "07/09/21",
		"description": "To update the my to do list with a different data structure",
		"status": "completed",
		"title": "Updated To Do List Item",
		"userId": 1
	})
})

.then(response => response.json())
.then(json => console.log(json));
